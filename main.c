#include<msp430x14x.h>
#include "lcd.h"
#include "portyLcd.h"
#define         BUTTON1                BIT4&P4IN   //definicja 4 guzik�w
#define         BUTTON2               BIT5&P4IN
#define         BUTTON3               BIT6&P4IN 
#define         BUTTON4              BIT7&P4IN 

//---------------- zmienne globalne -------------
unsigned int i=0;

unsigned int godziny1=0; //deklaracja zmiennych
unsigned int godziny2=0;
unsigned int minuty1=0;
unsigned int minuty2=0;
unsigned int sekundy1=0;
unsigned int sekundy2=0;
unsigned int milisekundy1=0;
unsigned int milisekundy2=0;
unsigned int start=0;


unsigned int licznik=0;

void Clock(void);

//----------------- main program -------------------
void main( void )
{
P2DIR |= BIT1 ;                   // STATUS LED
P2DIR &= BIT1 ;    
P1DIR &= BIT1; 

WDTCTL=WDTPW + WDTHOLD;           // Wylaczenie WDT

InitPortsLcd();                   // inicjalizacja port�w LCD
InitLCD();                        // inicjalizacja LCD
clearDisplay();                   // czyszczenie wyswietlacza      

// Basic Clock Module ustawiamy na ACLK(zegar 8 MHz ) i dzielimy czestotliwosc przez 2 (4 MHz)
BCSCTL1 |= XTS;                       // ACLK = LFXT1 = HF XTAL 8MHz

do 
  {
  IFG1 &= ~OFIFG;                     // Czyszczenie flgi OSCFault
  for (i = 0xFF; i > 0; i--);         // odczekanie
  }
  while ((IFG1 & OFIFG) == OFIFG);    // dop�ki OSCFault jest ciagle ustawiona   

BCSCTL1 |= DIVA_1;                    // ACLK=8 MHz/2=4 MHz
BCSCTL2 |= SELM0 | SELM1;             // MCLK= LFTX1 =ACLK

// Timer_A  ustawiamy na 500 kHz
// a przerwanie generujemy co 10 ms
TACTL = TASSEL_1 + MC_1 +ID_3;        // Wybieram ACLK, ACLK/8=500kHz,tryb Up
CCTL0 = CCIE;                         // wlaczenie przerwan od CCR0
CCR0=5000;                           // podzielnik 5000: przerwanie co 10 ms

_EINT();                              // wlaczenie przerwan

 
    godziny1=0;
    godziny2=0;
    minuty1=0;
    minuty2=0;
    sekundy1=0;
    sekundy2=0;
    milisekundy1=0;
    milisekundy2=-1;
    Clock();

for (;;)                //nieskonczona petla              
 {
 _BIS_SR(LPM3_bits);                  // przejscie do trybu LPM3

 
 if(start==1) //jezeli flaga start jest wyzerowana to..
 Clock(); //odmierzaj czas metoda Clock()
 
 if ((BUTTON1) == 0) // "zapisanie czasu okrazenia" na drugiej linii wyswietlacza
 {
   SEND_CMD(DD_RAM_ADDR2);
   printDecDigit(godziny1);   
   printDecDigit(godziny2); 
   SEND_CHAR(':');
   printDecDigit(minuty1);
   printDecDigit(minuty2); 
   SEND_CHAR(':');
   printDecDigit(sekundy1); 
   printDecDigit(sekundy2); 
   SEND_CHAR(':');
   printDecDigit(milisekundy1);
   printDecDigit(milisekundy2);
   SEND_CMD(DD_RAM_ADDR);
   SEND_CMD(CUR_HOME );
   
 }
 
  if ((BUTTON2) == 0) //start odmierzania czasu przez stoper
 {
   start=1;
 }
 
 if ((BUTTON3) == 0) //zatrzymanie odmierzania czasu
 {
   start=0;
 }
 
  if ((BUTTON4) == 0) //wyzerowanie wartosci czasu stopera
 {
    godziny1=0;
    godziny2=0;
    minuty1=0;
    minuty2=0;
    sekundy1=0;
    sekundy2=0;
    milisekundy1=0;
    milisekundy2=-1;
    Clock(); 
 }
 
  }
}

void Clock(void) //metoda odpowiedzialna za wyswietalnie czasu
{                
licznik=0;
P2OUT ^=BIT1;                           //zapal diode
   milisekundy2++;     //dodajemy 1 do wyswietlanej wartosci dziesietnych milisekund
   
   if(milisekundy2==10) //jezeli osiagniemy wartosc dwucyfrowa to..
   {
     milisekundy1++; //dodajemy 1 do wartosci setnych milisekundy
     milisekundy2=0; //zerujemy wartosc dziesietna milisekund
   }
   
   if(milisekundy1==10) //jezeli na wartosciach setnych otrzymamy 10 to..
   {
     sekundy2++; //dodajemy 1 do jednosci sekund
     milisekundy1=0; //zerujemy wartosc setna milisekund itd
   }
   
    if(sekundy2==10)
   {
     sekundy1++;
     sekundy2=0;
   }
   
     if(sekundy1==6)
   {
     minuty2++;
     sekundy1=0;
   }
   
     if(minuty2==10)
   {
     minuty1++;
     minuty2=0;
   }
   
     if(minuty1==6)
   {
     godziny1++;
     minuty1=0;
   }
   
     if(godziny1==10)
   {
     godziny2++;
     godziny1=0;
   }
  
   printDecDigit(godziny1);   //wypisujemy od lewej do prawej pojedyncze cyfry godziny, minuty, sekundy, milisekundy przedzielone znakiem ':'
   printDecDigit(godziny2); 
   SEND_CHAR(':');
   printDecDigit(minuty1);
   printDecDigit(minuty2); 
   SEND_CHAR(':');
   printDecDigit(sekundy1); 
   printDecDigit(sekundy2); 
   SEND_CHAR(':');
   printDecDigit(milisekundy1);
   printDecDigit(milisekundy2);
  
   SEND_CMD(CUR_HOME );           // wr�c kursorem na poczatek
}

// procedura obslugi przerwania od TimerA

#pragma vector=TIMERA0_VECTOR
__interrupt void Timer_A (void)
{
++licznik;
 _BIC_SR_IRQ(LPM3_bits);             // wyjscie z trybu LPM3
}